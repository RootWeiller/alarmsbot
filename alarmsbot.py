import telebot
import logging


TOKEN = TOKEN


# Enable Logging
logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

logger = logging.getLogger(__name__)


tb = telebot.TeleBot(TOKEN)
updates = tb.get_updates(1234,100,20)

last_chat_id = 0


def any_message(bot, update):
    """ Print to console """

    # Save last chat_id to use in reply handler
    global last_chat_id
    last_chat_id = update.message.chat_id

    logger.info("New message\nFrom: %s\nchat_id: %d\nText: %s" %
                (update.message.from_user,
                 update.message.chat_id,
                 update.message.text))


@tb.message_handler(commands=['saludo'])

def command_saludo(m):
	cid = m.chat.id
	tb.send_message(cid, message)

@tb.message_handler(commands=['guardia'])

def command_guardia(m):
	cid = m.chat.id
	guardia1 = 'Operador1'
	guardia2 = 'Operador2'

	tb.send_message(cid, 'Esta semana de Guardia' + guardia1)


@tb.message_handler(commands=['status'])

def command_status(m):
	cid = m.chat.id
	import os
	hostname = "www.google.com" 
	response = os.system("ping -c 1 " + hostname)
	
	if response == 0:
		tb.send_message(cid, 'Servicios Arriba')
	else:
		tb.send_message(cid, 'Alerta en los servicios')


@tb.message_handler(commands=['help'])

def command_help(m):
	cid = m.chat.id

	text_help = 'La lista de comandos es /guardia (para saber quien esta de guardia) \
	/saludo (recibes un saludo del bot) y /status (te da un estatus de la plataforma)'

	tb.send_message(cid, text_help)



tb.polling(none_stop=True)
